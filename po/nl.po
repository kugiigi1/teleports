# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the teleports.ubports package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: teleports.ubports\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2021-02-27 19:35+0000\n"
"PO-Revision-Date: 2021-03-04 18:27+0000\n"
"Last-Translator: Heimen Stoffels <vistausss@outlook.com>\n"
"Language-Team: Dutch <https://translate.ubports.com/projects/ubports/"
"teleports/nl/>\n"
"Language: nl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Weblate 3.11.3\n"

#: ../app/qml/components/CountryPicker.qml:21
#: ../app/qml/pages/WaitPhoneNumberPage.qml:36
msgid "Choose a country"
msgstr "Kies een land"

#: ../app/qml/components/CountryPicker.qml:38
msgid "Search country name..."
msgstr "Zoek een landnaam..."

#: ../app/qml/components/DeleteDialog.qml:9
msgid ""
"The message will be deleted for all users in the chat. Do you really want to "
"delete it?"
msgstr ""
"Het bericht wordt verwijderd voor alle gebruikers in de chat. Weet je zeker "
"dat je het wilt verwijderen?"

#: ../app/qml/components/DeleteDialog.qml:10
msgid ""
"The message will be deleted only for you. Do you really want to delete it?"
msgstr ""
"Het bericht wordt alleen voor jou verwijderd. Weet je zeker dat je het wilt "
"verwijderen?"

#: ../app/qml/components/DeleteDialog.qml:12
#: ../app/qml/delegates/MessageBubbleItem.qml:37
#: ../app/qml/pages/SettingsPage.qml:152 ../app/qml/pages/UserListPage.qml:113
#: ../app/qml/pages/UserListPage.qml:207
msgid "Delete"
msgstr "Verwijderen"

#: ../app/qml/components/GroupPreviewDialog.qml:35
#: ../app/qml/pages/MessageListPage.qml:33
msgid "%1 member"
msgid_plural "%1 members"
msgstr[0] "%1 lid"
msgstr[1] "%1 leden"

#: ../app/qml/components/GroupPreviewDialog.qml:37
msgid "%1 members, among them:"
msgstr "%1 leden, onder wie:"

#: ../app/qml/components/InputInfoBox.qml:91
msgid "Edit message"
msgstr "Bericht aanpassen"

#: ../app/qml/components/MessageStatusRow.qml:36
msgid "Edited"
msgstr "Bewerkt"

#: ../app/qml/components/PopupDialog.qml:14
msgid "Okay"
msgstr "Oké"

#: ../app/qml/components/PopupDialog.qml:15
#: ../app/qml/components/PopupWaitCancel.qml:13
#: ../app/qml/pages/ChatListPage.qml:32
msgid "Cancel"
msgstr "Annuleren"

#: ../app/qml/components/UserProfile.qml:71
msgid "Members: %1"
msgstr "Leden: %1"

#: ../app/qml/delegates/MessageBasicGroupChatCreate.qml:6
msgid "Channel called <b>%1</b> created"
msgstr "Kanaal genaamd <b>%1</b> aangemaakt"

#: ../app/qml/delegates/MessageBasicGroupChatCreate.qml:7
msgid "%1 created a group called <b>%2</b>"
msgstr "%1 heeft een groep aangemaakt met de naam <b>%2</b>"

#: ../app/qml/delegates/MessageBubbleItem.qml:49
msgid "Copy"
msgstr "Kopiëren"

#: ../app/qml/delegates/MessageBubbleItem.qml:59
#: ../app/qml/pages/ChatInfoPage.qml:35
msgid "Edit"
msgstr "Bewerken"

#: ../app/qml/delegates/MessageBubbleItem.qml:67
msgid "Reply"
msgstr "Antwoorden"

#: ../app/qml/delegates/MessageBubbleItem.qml:73
msgid "Sticker Pack info"
msgstr "Stickerpakketinformatie"

#: ../app/qml/delegates/MessageBubbleItem.qml:79
#: ../app/qml/pages/ChatListPage.qml:438
msgid "Forward"
msgstr "Doorsturen"

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:8
msgid "%1 joined the group"
msgstr "%1 is lid geworden van de groep"

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:9
msgid "%1 added %2"
msgstr "%1 heeft %2 toegevoegd"

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:10
msgid "Unknown joined group"
msgstr "Een onbekende is lid geworden van de groep"

#: ../app/qml/delegates/MessageChatAddMembersItem.qml:32
msgid "%1 user(s)"
msgid_plural ""
msgstr[0] "%1 gebruiker"
msgstr[1] "%1 gebruikers"

#: ../app/qml/delegates/MessageChatChangePhoto.qml:32
msgid "Channel photo has been changed:"
msgstr "De kanaalfoto is gewijzigd:"

#: ../app/qml/delegates/MessageChatChangePhoto.qml:33
msgid "%1 changed the chat photo:"
msgstr "%1 heeft de chatfoto gewijzigd:"

#: ../app/qml/delegates/MessageChatChangeTitle.qml:6
msgid "Channel title has been changed to <b>%1</b>"
msgstr "De kanaalnaam is gewijzigd in <b>%1</b>"

#: ../app/qml/delegates/MessageChatChangeTitle.qml:7
msgid "%1 changed the chat title to <b>%2</b>"
msgstr "de chatnaam is gewijzigd in <b>%2</b>"

#: ../app/qml/delegates/MessageChatDeleteMember.qml:6
msgid "%1 left the group"
msgstr "%1 heeft de groep verlaten"

#. TRANSLATORS: Notification message saying: person A removed person B (from a group)
#: ../app/qml/delegates/MessageChatDeleteMember.qml:7
#: ../push/pushhelper.cpp:238
msgid "%1 removed %2"
msgstr "%1 heeft %2 verwijderd"

#: ../app/qml/delegates/MessageChatDeletePhoto.qml:6
msgid "Channel photo has been removed"
msgstr "De kanaalfoto is verwijderd"

#: ../app/qml/delegates/MessageChatDeletePhoto.qml:7
msgid "%1 deleted the chat photo"
msgstr "%1 heeft de chatfoto verwijderd"

#: ../app/qml/delegates/MessageChatSetTTL.qml:4
msgid "Message time-to-live has been set to <b>%1</b> seconds"
msgstr "De berichtzichtbaarheid is ingesteld op <b>%1</b> seconden"

#: ../app/qml/delegates/MessageChatUpgradeFrom.qml:4
#: ../app/qml/delegates/MessageChatUpgradeTo.qml:4
msgid "Group has been upgraded to Supergroup"
msgstr "De groep is opgewaardeerd tot supergroep"

#: ../app/qml/delegates/MessageContactRegistered.qml:5
msgid "%1 has joined Telegram!"
msgstr "%1 heeft nu ook Telegram!"

#: ../app/qml/delegates/MessageContentBase.qml:37
msgid "Forwarded from %1"
msgstr "Doorgestuurd door %1"

#. TRANSLATORS: This is the duration of a phone call in hours:minutes:seconds format
#: ../app/qml/delegates/MessageContentCall.qml:59
msgid "Duration: %1:%2:%3"
msgstr "Duur: %1:%2:%3"

#: ../app/qml/delegates/MessageContentVoiceNote.qml:56
msgid "Voice note"
msgstr "Spraaknotitie"

#: ../app/qml/delegates/MessageDateItem.qml:4
msgid "Some date missing"
msgstr "De datum ontbreekt"

#: ../app/qml/delegates/MessageJoinByLink.qml:5
msgid "%1 joined by invite link"
msgstr "%1 is lid geworden via een uitnodigingslink"

#: ../app/qml/delegates/MessagePinMessage.qml:6
#: ../app/qml/delegates/MessagePinMessage.qml:7 ../push/pushhelper.cpp:301
msgid "%1 pinned a message"
msgstr "%1 heeft een bericht vastgezet"

#: ../app/qml/delegates/MessageScreenshotTaken.qml:4
msgid "A screenshot has been taken"
msgstr "Er is een schermfoto gemaakt"

#: ../app/qml/delegates/MessageStickerItem.qml:20
msgid "Animated stickers not supported yet :("
msgstr "Bewegende stickers worden nog niet ondersteund :("

#: ../app/qml/delegates/MessageUnavailable.qml:11
msgid "Message Unavailable..."
msgstr "Het bericht is niet beschikbaar..."

#: ../app/qml/delegates/MessageUnreadLabelItem.qml:4
msgid "Missing label..."
msgstr "Ontbrekend label..."

#: ../app/qml/delegates/MessageUnsupported.qml:4
#: ../libs/qtdlib/messages/content/qtdmessageunsupported.cpp:12
msgid "Unsupported message"
msgstr "Niet-ondersteund bericht"

#: ../app/qml/delegates/NotImplementedYet.qml:12
msgid "Unknown message type, see logfile for details..."
msgstr "Onbekend soort berichttype - bekijk het logbestand..."

#: ../app/qml/middleware/ChatMiddleware.qml:27
msgid "Are you sure you want to clear the history?"
msgstr "Weet je zeker dat je de geschiedenis wilt wissen?"

#: ../app/qml/middleware/ChatMiddleware.qml:28
#: ../app/qml/pages/ChatListPage.qml:208
msgid "Clear history"
msgstr "Geschiedenis wissen"

#: ../app/qml/middleware/ChatMiddleware.qml:37
msgid "Are you sure you want to leave this chat?"
msgstr "Weet je zeker dat je deze chat wilt verlaten?"

#: ../app/qml/middleware/ChatMiddleware.qml:38
msgid "Leave"
msgstr "Verlaten"

#: ../app/qml/middleware/ChatMiddleware.qml:47
msgid "Join group"
msgstr "Lid worden"

#: ../app/qml/middleware/ErrorsMiddleware.qml:19
msgid "Close"
msgstr "Sluiten"

#: ../app/qml/pages/AboutPage.qml:13 ../app/qml/pages/ChatListPage.qml:154
msgid "About"
msgstr "Over"

#: ../app/qml/pages/AboutPage.qml:24 ../app/qml/pages/ChatInfoPage.qml:26
#: ../app/qml/pages/ConnectivityPage.qml:46
#: ../app/qml/pages/SecretChatKeyHashPage.qml:18
#: ../app/qml/pages/SettingsPage.qml:33 ../app/qml/pages/UserListPage.qml:29
msgid "Back"
msgstr "Vorige"

#. TRANSLATORS: Application name.
#: ../app/qml/pages/AboutPage.qml:66 ../push/pushhelper.cpp:114
#: teleports.desktop.in.h:1
msgid "TELEports"
msgstr "TELEports"

#: ../app/qml/pages/AboutPage.qml:72
msgid "Version %1"
msgstr "Versie %1"

#: ../app/qml/pages/AboutPage.qml:74
msgid " (git# %1)"
msgstr " (git# %1)"

#: ../app/qml/pages/AboutPage.qml:93
msgid "Get the source"
msgstr "Broncode bekijken"

#: ../app/qml/pages/AboutPage.qml:94
msgid "Report issues"
msgstr "Problemen melden"

#: ../app/qml/pages/AboutPage.qml:95
msgid "Help translate"
msgstr "Helpen met vertalen"

#: ../app/qml/pages/ChatInfoPage.qml:21
msgid "Group Details"
msgstr "Groepsinformatie"

#: ../app/qml/pages/ChatInfoPage.qml:21
msgid "Profile"
msgstr "Profiel"

#: ../app/qml/pages/ChatInfoPage.qml:41
msgid "Send message"
msgstr "Bericht versturen"

#: ../app/qml/pages/ChatInfoPage.qml:61
msgid "Edit user data and press Save"
msgstr "Wijzig de gebruikersgegevens en druk op 'Opslaan'"

#: ../app/qml/pages/ChatInfoPage.qml:63 ../app/qml/pages/PreviewPage.qml:49
msgid "Save"
msgstr "Opslaan"

#: ../app/qml/pages/ChatInfoPage.qml:74 ../app/qml/pages/UserListPage.qml:59
msgid "Phone no"
msgstr "Telefoonnr."

#: ../app/qml/pages/ChatInfoPage.qml:83 ../app/qml/pages/UserListPage.qml:67
msgid "First name"
msgstr "Voornaam"

#: ../app/qml/pages/ChatInfoPage.qml:92 ../app/qml/pages/UserListPage.qml:75
msgid "Last name"
msgstr "Achternaam"

#: ../app/qml/pages/ChatInfoPage.qml:131
msgid "Notifications"
msgstr "Meldingen"

#: ../app/qml/pages/ChatInfoPage.qml:160
msgid "%1 group in common"
msgid_plural "%1 groups in common"
msgstr[0] "Beide lid van %1 groep"
msgstr[1] "Beide lid van %1 groepen"

#: ../app/qml/pages/ChatInfoPage.qml:185
#: ../app/qml/pages/SecretChatKeyHashPage.qml:13
msgid "Encryption Key"
msgstr "Toegangssleutel"

#: ../app/qml/pages/ChatListPage.qml:21
msgid "Select destination or cancel..."
msgstr "Kies een locatie of annuleer..."

#: ../app/qml/pages/ChatListPage.qml:39 ../app/qml/pages/ChatListPage.qml:135
#: ../app/qml/pages/SettingsPage.qml:22
msgid "Settings"
msgstr "Instellingen"

#: ../app/qml/pages/ChatListPage.qml:62
msgid "Search"
msgstr "Zoeken"

#: ../app/qml/pages/ChatListPage.qml:112
msgid "All"
msgstr "Alles"

#: ../app/qml/pages/ChatListPage.qml:112
msgid "Archived"
msgstr "Gearchiveerd"

#: ../app/qml/pages/ChatListPage.qml:112
msgid "Personal"
msgstr "Persoonlijk"

#: ../app/qml/pages/ChatListPage.qml:112
msgid "Unread"
msgstr "Ongelezen"

#: ../app/qml/pages/ChatListPage.qml:125 ../libs/qtdlib/chat/qtdchat.cpp:87
msgid "Saved Messages"
msgstr "Opgeslagen berichten"

#: ../app/qml/pages/ChatListPage.qml:130 ../app/qml/pages/UserListPage.qml:18
msgid "Contacts"
msgstr "Contactpersonen"

#: ../app/qml/pages/ChatListPage.qml:141
msgid "Night mode"
msgstr "Nachtmodus"

#: ../app/qml/pages/ChatListPage.qml:203
msgid "Leave chat"
msgstr "Chat verlaten"

#: ../app/qml/pages/ChatListPage.qml:219 ../app/qml/pages/UserListPage.qml:123
msgid "Info"
msgstr "Informatie"

#: ../app/qml/pages/ChatListPage.qml:436
msgid "Do you want to forward the selected messages to %1?"
msgstr "Wil je de geselecteerde berichten doorsturen naar %1?"

#: ../app/qml/pages/ChatListPage.qml:451 ../app/qml/pages/ChatListPage.qml:476
msgid "Enter optional message..."
msgstr "Voer een bericht in (optioneel)..."

#: ../app/qml/pages/ChatListPage.qml:459
msgid "Do you want to send the imported files to %1?"
msgstr "Wil je de geïmporteerde berichten versturen naar %1?"

#: ../app/qml/pages/ChatListPage.qml:460
msgid "Do you want to send the imported text to %1?"
msgstr "Wil je de geïmporteerde tekst versturen naar %1?"

#: ../app/qml/pages/ChatListPage.qml:462
#: ../app/qml/pages/MessageListPage.qml:855
msgid "Send"
msgstr "Versturen"

#: ../app/qml/pages/ConnectivityPage.qml:24
#: ../app/qml/pages/ConnectivityPage.qml:28
msgid "Connecting"
msgstr "Bezig met verbinden"

#: ../app/qml/pages/ConnectivityPage.qml:24
#: ../app/qml/pages/ConnectivityPage.qml:26
msgid "Offline"
msgstr "Offline"

#: ../app/qml/pages/ConnectivityPage.qml:24
#: ../app/qml/pages/ConnectivityPage.qml:30
#: ../libs/qtdlib/user/qtduserstatus.cpp:78
msgid "Online"
msgstr "Online"

#: ../app/qml/pages/ConnectivityPage.qml:27
msgid "Connecting To Proxy"
msgstr "Bezig met verbinden met proxy"

#: ../app/qml/pages/ConnectivityPage.qml:29
msgid "Updating"
msgstr "Bezig met bijwerken"

#: ../app/qml/pages/ConnectivityPage.qml:35
msgid "Connectivity"
msgstr "Verbinding"

#: ../app/qml/pages/ConnectivityPage.qml:74
msgid "Telegram connectivity status:"
msgstr "Telegram-verbindingsstatus:"

#: ../app/qml/pages/ConnectivityPage.qml:81
msgid "Ubuntu Touch connectivity status:"
msgstr "Ubuntu Touch-verbindingsstatus:"

#: ../app/qml/pages/ConnectivityPage.qml:88
msgid "Ubuntu Touch bandwith limited"
msgstr "De bandbreedte van Ubuntu Touch is beperkt"

#: ../app/qml/pages/ConnectivityPage.qml:88
msgid "Ubuntu Touch bandwith not limited"
msgstr "De bandbreedte van Ubuntu Touch is niet beperkt"

#: ../app/qml/pages/LogoutPage.qml:14
msgid "Good bye!"
msgstr "Tot ziens!"

#: ../app/qml/pages/LogoutPage.qml:27
msgid "Disconnecting..."
msgstr "Bezig met verbreken van verbinding..."

#: ../app/qml/pages/LogoutPage.qml:37
msgid ""
"The app will close automatically when the logout process ends.\n"
"Please, don't close it manually!"
msgstr ""
"De app wordt automatisch gesloten als het uitlogproces is afgerond.\n"
"Sluit de app niet handmatig af!"

#: ../app/qml/pages/MessageListPage.qml:35
msgid ", %1 online"
msgid_plural ", %1 online"
msgstr[0] "- %1 online"
msgstr[1] "- %1 online"

#: ../app/qml/pages/MessageListPage.qml:132
msgid "Telegram"
msgstr "Telegram"

#: ../app/qml/pages/MessageListPage.qml:338
msgid "You are not allowed to post in this channel"
msgstr "Je hebt geen toestemming om berichten te plaatsen in deze chat"

#: ../app/qml/pages/MessageListPage.qml:342
msgid "Waiting for other party to accept the secret chat..."
msgstr "Bezig met wachten op de ander om de geheime chat te accepteren..."

#: ../app/qml/pages/MessageListPage.qml:344
msgid "Secret chat has been closed"
msgstr "De geheime chat is beëindigd"

#: ../app/qml/pages/MessageListPage.qml:352
msgid "You left this group"
msgstr "Je hebt de groep verlaten"

#: ../app/qml/pages/MessageListPage.qml:355
msgid "You have been banned"
msgstr "Je bent verbannen"

#: ../app/qml/pages/MessageListPage.qml:358
msgid "You are not allowed to post in this group"
msgstr "Je hebt geen toestemming om berichten te plaatsen in deze groep"

#: ../app/qml/pages/MessageListPage.qml:362
msgid "You can't write here. Reason unkown"
msgstr "Om onbekende reden kun je hier niks schrijven"

#: ../app/qml/pages/MessageListPage.qml:385
msgid "Join"
msgstr "Lid worden"

#: ../app/qml/pages/MessageListPage.qml:525
msgid "Type a message..."
msgstr "Typ een bericht..."

#: ../app/qml/pages/MessageListPage.qml:743
msgid "<<< Swipe to cancel"
msgstr "<<< Veeg om af te breken"

#: ../app/qml/pages/MessageListPage.qml:853
msgid "Do you want to share your location with %1?"
msgstr "Wil je je locatie delen met %1?"

#: ../app/qml/pages/MessageListPage.qml:866
msgid "Requesting location from OS..."
msgstr "Bezig met opvragen van locatie..."

#: ../app/qml/pages/PickerPage.qml:16
msgid "Content Picker"
msgstr "Bestandskiezer"

#: ../app/qml/pages/PreviewPage.qml:35
msgid "File: "
msgstr "Bestand: "

#: ../app/qml/pages/SecretChatKeyHashPage.qml:65
msgid ""
"Check the image or the text. If they match with the ones on <b>%1</b>'s "
"device, end-to-end cryptography is granted."
msgstr ""
"Controleer de afbeelding of tekst. Als deze overeenkomt met die op <b>%1</"
"b>'s apparaat, dan is er gegarandeerd end-to-endversleuteling."

#: ../app/qml/pages/SettingsPage.qml:70 ../app/qml/pages/SettingsPage.qml:142
msgid "Logout"
msgstr "Uitloggen"

#: ../app/qml/pages/SettingsPage.qml:86
msgid "Delete account"
msgstr "Account verwijderen"

#: ../app/qml/pages/SettingsPage.qml:102
msgid "Connectivity status"
msgstr "Verbindingsstatus"

#: ../app/qml/pages/SettingsPage.qml:119
msgid "Toggle message status indicators"
msgstr "Berichtstatusindicators tonen/verbergen"

#: ../app/qml/pages/SettingsPage.qml:140
msgid ""
"Warning: Logging out will delete all local data from this device, including "
"secret chats. Are you still sure you want to log out?"
msgstr ""
"Waarschuwing: door uit te loggen worden alle lokaal opgeslagen gegevens, "
"inclusief geheime chats, verwijderd. Weet je zeker dat je wilt uitloggen?"

#: ../app/qml/pages/SettingsPage.qml:150
msgid ""
"Warning: Deleting the account will delete all the data you ever received or "
"sent using telegram except for data you have explicitly saved outside the "
"telegram cloud. Are you really really sure you want to delete your telegram "
"account?"
msgstr ""
"Waarschuwing: door je account te verwijderen worden alle gegevens die je "
"ooit met Telegram ontvangen of verzonden hebt verwijderd. Dit geldt niet "
"voor gegevens die je buiten de Telegram-servers om hebt opgeslagen. Weet je "
"heel, heel, héél erg zeker dat je je Telegram-account wilt verwijderen?"

#: ../app/qml/pages/UserListPage.qml:38
msgid "Add Contact"
msgstr "Contactpersoon toevoegen"

#: ../app/qml/pages/UserListPage.qml:47
msgid "The contact will be added. First and last name are optional"
msgstr "De contactpersoon wordt toegevoegd; voor- en achternaam zijn optioneel"

#: ../app/qml/pages/UserListPage.qml:49
msgid "Add"
msgstr "Toevoegen"

#: ../app/qml/pages/UserListPage.qml:128
msgid "Secret Chat"
msgstr "Geheime chat"

#: ../app/qml/pages/UserListPage.qml:205
msgid "The contact will be deleted. Are you sure?"
msgstr "De contactpersoon wordt verwijderd. Weet je het zeker?"

#: ../app/qml/pages/WaitCodePage.qml:17
msgid "Enter Code"
msgstr "Voer de code in"

#: ../app/qml/pages/WaitCodePage.qml:39
msgid "Code"
msgstr "Code"

#: ../app/qml/pages/WaitCodePage.qml:54
msgid ""
"A code was sent via Telegram to your other devices. Please enter it here."
msgstr ""
"Voer hier de code in die je van Telegram op je andere apparaten hebt "
"ontvangen."

#: ../app/qml/pages/WaitPasswordPage.qml:18
msgid "Enter Password"
msgstr "Voer je wachtwoord in"

#: ../app/qml/pages/WaitPasswordPage.qml:39
msgid "Password"
msgstr "Wachtwoord"

#: ../app/qml/pages/WaitPasswordPage.qml:57
msgid "Password hint: %1"
msgstr "Wachtwoordhint: %1"

#: ../app/qml/pages/WaitPasswordPage.qml:62
#: ../app/qml/pages/WaitPhoneNumberPage.qml:92
#: ../app/qml/pages/WaitRegistrationPage.qml:57
msgid "Next..."
msgstr "Volgende..."

#: ../app/qml/pages/WaitPhoneNumberPage.qml:17
msgid "Enter Phone Number"
msgstr "Voer hier je telefoonnummer in"

#: ../app/qml/pages/WaitPhoneNumberPage.qml:54
#: ../app/qml/pages/WaitPhoneNumberPage.qml:64
msgid "Phone number"
msgstr "Telefoonnummer"

#: ../app/qml/pages/WaitPhoneNumberPage.qml:88
msgid "Please confirm your country code and enter your phone number."
msgstr "Bevestig je landnummer en voer je telefoonnummer in."

#: ../app/qml/pages/WaitRegistrationPage.qml:17
msgid "Enter your Name"
msgstr "Voer hier je naam in"

#: ../app/qml/pages/WaitRegistrationPage.qml:38
msgid "First Name"
msgstr "Voornaam"

#: ../app/qml/pages/WaitRegistrationPage.qml:44
msgid "Last Name"
msgstr "Achternaam"

#: ../app/qml/stores/AuthStateStore.qml:60
msgid "Invalid phone number!"
msgstr "Ongeldig telefoonnummer!"

#: ../app/qml/stores/AuthStateStore.qml:67
msgid "Invalid code!"
msgstr "Ongeldige code!"

#: ../app/qml/stores/AuthStateStore.qml:74
msgid "Invalid password!"
msgstr "Ongeldig wachtwoord!"

#: ../app/qml/stores/AuthStateStore.qml:94
msgid "Auth code not expected right now"
msgstr "Authorisatiecode nu niet verwacht"

#: ../app/qml/stores/AuthStateStore.qml:100
msgid "Oops! Internal error."
msgstr "Oeps! Interne fout."

#: ../app/qml/stores/AuthStateStore.qml:119
msgid "Registration not expected right now"
msgstr "Registratie nu niet verwacht"

#: ../app/qml/stores/ChatStateStore.qml:42
#: ../app/qml/stores/ChatStateStore.qml:48
msgid "Error"
msgstr "Fout"

#: ../app/qml/stores/ChatStateStore.qml:42
msgid "No valid location received after 180 seconds!"
msgstr "Geen geldige locatie ontvangen na 180 seconden!"

#: ../app/qml/stores/ChatStateStore.qml:48
msgid "Username <b>@%1</b> not found"
msgstr "De gebruiker <b>@%1</b> is niet gevonden"

#: ../app/qml/stores/NotificationsStateStore.qml:12
msgid "Push Registration Failed"
msgstr "Pushregistratie mislukt"

#: ../app/qml/stores/NotificationsStateStore.qml:23
msgid "No Ubuntu One"
msgstr "Geen Ubuntu One"

#: ../app/qml/stores/NotificationsStateStore.qml:24
msgid "Please connect to Ubuntu One to receive push notifications."
msgstr "Koppel je Ubuntu One-account om pushmeldingen te ontvangen."

#: ../libs/qtdlib/chat/qtdchat.cpp:326
msgid "Draft:"
msgstr "Concept:"

#: ../libs/qtdlib/chat/qtdchat.cpp:622
msgid "is choosing contact..."
msgstr "kiest een contactpersoon..."

#: ../libs/qtdlib/chat/qtdchat.cpp:623
msgid "are choosing contact..."
msgstr "kiezen een contactpersoon..."

#: ../libs/qtdlib/chat/qtdchat.cpp:626
msgid "is choosing location..."
msgstr "kiest een locatie..."

#: ../libs/qtdlib/chat/qtdchat.cpp:627
msgid "are choosing location..."
msgstr "kiezen een locatie..."

#: ../libs/qtdlib/chat/qtdchat.cpp:632
msgid "is recording..."
msgstr "is aan het opnemen..."

#: ../libs/qtdlib/chat/qtdchat.cpp:633
msgid "are recording..."
msgstr "zijn aan het opnemen..."

#: ../libs/qtdlib/chat/qtdchat.cpp:636
msgid "is typing..."
msgstr "is aan het typen..."

#: ../libs/qtdlib/chat/qtdchat.cpp:637
msgid "are typing..."
msgstr "zijn aan het typen..."

#: ../libs/qtdlib/chat/qtdchat.cpp:640
msgid "is doing something..."
msgstr "is iets aan het doen..."

#: ../libs/qtdlib/chat/qtdchat.cpp:641
msgid "are doing something..."
msgstr "zijn iets aan het doen..."

#: ../libs/qtdlib/messages/content/qtdmessageanimation.cpp:25
msgid "GIF"
msgstr "GIF"

#: ../libs/qtdlib/messages/content/qtdmessageanimation.cpp:25
msgid "GIF,"
msgstr "GIF,"

#: ../libs/qtdlib/messages/content/qtdmessagebasicgroupchatcreate.cpp:25
#: ../libs/qtdlib/messages/content/qtdmessagesupergroupchatcreate.cpp:23
msgid "created this group"
msgstr "heeft deze groep aangemaakt"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:68
msgid "Call Declined"
msgstr "Oproep geweigerd"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:71
msgid "Call Disconnected"
msgstr "Verbinding verbroken"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:74
msgid "Call Ended"
msgstr "Oproep beëindigd"

#. TRANSLATORS: This is a duration in hours:minutes:seconds format - only arrange the order, do not translate!
#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:78
msgid "%1:%2:%3"
msgstr "%1:%2:%3"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:79
msgid "Outgoing Call (%1)"
msgstr "Uitgaande oproep (%1)"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:80
msgid "Incoming Call (%1)"
msgstr "Inkomende oproep (%1)"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:83
msgid "Cancelled Call"
msgstr "Afgebroken oproep"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:83
msgid "Missed Call"
msgstr "Gemiste oproep"

#: ../libs/qtdlib/messages/content/qtdmessagecall.cpp:86
msgid "Call"
msgstr "Oproep"

#: ../libs/qtdlib/messages/content/qtdmessagechataddmembers.cpp:34
msgid "added one or more members"
msgstr "heeft één of meer leden toegevoegd"

#: ../libs/qtdlib/messages/content/qtdmessagechataddmembers.cpp:34
msgid "joined the group"
msgstr "is lid geworden van de groep"

#: ../libs/qtdlib/messages/content/qtdmessagechatchangephoto.cpp:20
msgid "changed the chat photo"
msgstr "heeft de chatfoto gewijzigd"

#: ../libs/qtdlib/messages/content/qtdmessagechatchangetitle.cpp:19
msgid "changed the chat title"
msgstr "heeft de chatnaam gewijzigd"

#: ../libs/qtdlib/messages/content/qtdmessagechatdeletemember.cpp:32
msgid "left the group"
msgstr "heeft de groep verlaten"

#: ../libs/qtdlib/messages/content/qtdmessagechatdeletemember.cpp:32
msgid "removed a member"
msgstr "heeft een lid verwijderd"

#: ../libs/qtdlib/messages/content/qtdmessagechatdeletephoto.cpp:8
msgid "deleted the chat photo"
msgstr "heeft de chatfoto verwijderd"

#: ../libs/qtdlib/messages/content/qtdmessagechatjoinbylink.cpp:8
msgid "joined the group via the public link"
msgstr "is lid geworden van de groep via een openbare link"

#: ../libs/qtdlib/messages/content/qtdmessagechatsetttl.cpp:19
msgid "message TTL has been changed"
msgstr "Het TTL-bericht is gewijzigd"

#: ../libs/qtdlib/messages/content/qtdmessagechatupgradefrom.cpp:29
#: ../libs/qtdlib/messages/content/qtdmessagechatupgradeto.cpp:29
msgid "upgraded to supergroup"
msgstr "opgewaardeerd naar supergroep"

#: ../libs/qtdlib/messages/content/qtdmessagecontact.cpp:18
msgid "Contact"
msgstr "Contactpersoon"

#: ../libs/qtdlib/messages/content/qtdmessagecontactregistered.cpp:9
msgid "has joined Telegram!"
msgstr "heeft nu ook Telegram!"

#: ../libs/qtdlib/messages/content/qtdmessagedate.cpp:19
msgid "Today"
msgstr "Vandaag"

#: ../libs/qtdlib/messages/content/qtdmessagedate.cpp:21
msgid "Yesterday"
msgstr "Gisteren"

#. TRANSLATORS: String in date separator label. For messages within a week: full weekday name
#: ../libs/qtdlib/messages/content/qtdmessagedate.cpp:24
msgid "dddd"
msgstr "dddd"

#. TRANSLATORS: String in date separator label. For messages of pas years: date number, month name and year
#: ../libs/qtdlib/messages/content/qtdmessagedate.cpp:27
msgid "dd MMMM yyyy"
msgstr "dd MMMM yyyy"

#. TRANSLATORS: String in date separator label. For messages older that a week but within the current year: date number and month name
#: ../libs/qtdlib/messages/content/qtdmessagedate.cpp:30
msgid "dd MMMM"
msgstr "dd MMMM"

#: ../libs/qtdlib/messages/content/qtdmessagelocation.cpp:19
msgid "Location"
msgstr "Locatie"

#: ../libs/qtdlib/messages/content/qtdmessagephoto.cpp:26
msgid "Photo"
msgstr "Foto"

#: ../libs/qtdlib/messages/content/qtdmessagephoto.cpp:26
msgid "Photo,"
msgstr "Foto,"

#: ../libs/qtdlib/messages/content/qtdmessagepinmessage.cpp:19
msgid "Pinned Message"
msgstr "Vastgemaakt berichten"

#: ../libs/qtdlib/messages/content/qtdmessagesticker.cpp:20
msgid "Sticker"
msgstr "Sticker"

#: ../libs/qtdlib/messages/content/qtdmessagevideo.cpp:25
msgid "Video"
msgstr "Video"

#: ../libs/qtdlib/messages/content/qtdmessagevideo.cpp:25
msgid "Video,"
msgstr "Video,"

#: ../libs/qtdlib/messages/content/qtdmessagevideonote.cpp:30
msgid "Video message"
msgstr "Videobericht"

#: ../libs/qtdlib/messages/content/qtdmessagevoicenote.cpp:31
msgid "Voice message"
msgstr "Spraakbericht"

#: ../libs/qtdlib/messages/content/qtdmessagevoicenote.cpp:31
msgid "Voice message,"
msgstr "Spraakbericht,"

#: ../libs/qtdlib/messages/qtdmessage.cpp:84
msgid "Me"
msgstr "Ik"

#: ../libs/qtdlib/messages/qtdmessagecontentfactory.cpp:133
msgid "Unimplemented:"
msgstr "Niet geïmplementeerd:"

#: ../libs/qtdlib/messages/qtdmessagelistmodel.cpp:250
msgid "Unread Messages"
msgstr "Ongelezen berichten"

#: ../libs/qtdlib/user/qtduserstatus.cpp:28
msgid "Last seen one month ago"
msgstr "Laatst gezien: één maand geleden"

#: ../libs/qtdlib/user/qtduserstatus.cpp:39
msgid "Last seen one week ago"
msgstr "Laatst gezien: één week geleden"

#: ../libs/qtdlib/user/qtduserstatus.cpp:55
msgid "Last seen "
msgstr "Laatst gezien: "

#: ../libs/qtdlib/user/qtduserstatus.cpp:55
msgid "dd.MM.yy hh:mm"
msgstr "dd-MM-yy hh:mm"

#: ../libs/qtdlib/user/qtduserstatus.cpp:96
msgid "Seen recently"
msgstr "Onlangs gezien"

#: ../push/pushhelper.cpp:124
msgid "sent you a message"
msgstr "heeft je een bericht gestuurd"

#: ../push/pushhelper.cpp:128
msgid "sent you a photo"
msgstr "heeft je een foto gestuurd"

#: ../push/pushhelper.cpp:132
msgid "sent you a sticker"
msgstr "heeft je een sticker gestuurd"

#: ../push/pushhelper.cpp:136
msgid "sent you a video"
msgstr "heeft je een video gestuurd"

#: ../push/pushhelper.cpp:140
msgid "sent you a document"
msgstr "heeft je een document gestuurd"

#: ../push/pushhelper.cpp:144
msgid "sent you an audio message"
msgstr "heeft je een geluidsfragment gestuurd"

#: ../push/pushhelper.cpp:148
msgid "sent you a voice message"
msgstr "heeft je een spraakbericht gestuurd"

#: ../push/pushhelper.cpp:152
msgid "shared a contact with you"
msgstr "heeft een contactpersoon met je gedeeld"

#: ../push/pushhelper.cpp:156
msgid "sent you a map"
msgstr "heeft je een locatie gestuurd"

#: ../push/pushhelper.cpp:161
msgid "%1: %2"
msgstr "%1: %2"

#: ../push/pushhelper.cpp:166
msgid "%1 sent a message to the group"
msgstr "%1 heeft een bericht naar de groep gestuurd"

#: ../push/pushhelper.cpp:171
msgid "%1 sent a photo to the group"
msgstr "%1 heeft een foto naar de groep gestuurd"

#: ../push/pushhelper.cpp:176
msgid "%1 sent a sticker to the group"
msgstr "%1 heeft een sticker naar de groep gestuurd"

#: ../push/pushhelper.cpp:181
msgid "%1 sent a video to the group"
msgstr "%1 heeft een video naar de groep gestuurd"

#: ../push/pushhelper.cpp:186
msgid "%1 sent a document to the group"
msgstr "%1 heeft een document naar de groep gestuurd"

#: ../push/pushhelper.cpp:191
msgid "%1 sent a voice message to the group"
msgstr "%1 heeft een spraakbericht naar de groep gestuurd"

#: ../push/pushhelper.cpp:196
msgid "%1 sent a GIF to the group"
msgstr "%1 heeft een GIF met de groep gedeeld"

#: ../push/pushhelper.cpp:201
msgid "%1 sent a contact to the group"
msgstr "%1 heeft een contactpersoon met de groep gedeeld"

#: ../push/pushhelper.cpp:206
msgid "%1 sent a map to the group"
msgstr "%1 heeft een locatie met de groep gedeeld"

#: ../push/pushhelper.cpp:211 ../push/pushhelper.cpp:232
msgid "%1 invited you to the group"
msgstr "%1 heeft je uitgenodigd voor de groep"

#: ../push/pushhelper.cpp:216
msgid "%1 changed group name"
msgstr "%1 heeft de groepsnaam gewijzigd"

#: ../push/pushhelper.cpp:221
msgid "%1 changed group photo"
msgstr "%1 heeft de groepsfoto gewijzigd"

#. TRANSLATORS: Notification message saying: person A invited person B (to a group)
#: ../push/pushhelper.cpp:227
msgid "%1 invited %2"
msgstr "%1 heeft %2 uitgenodigd"

#: ../push/pushhelper.cpp:243
msgid "%1 removed you from the group"
msgstr "%1 heeft je uit de groep verwijderd"

#: ../push/pushhelper.cpp:248
msgid "%1 has left the group"
msgstr "%1 heeft de groep verlaten"

#: ../push/pushhelper.cpp:253
msgid "%1 has returned to the group"
msgstr "%1 is teruggekeerd in de groep"

#. TRANSLATORS: This format string tells location, like: @ McDonals, New York
#: ../push/pushhelper.cpp:258
msgid "@ %1"
msgstr "@ %1"

#. TRANSLATORS: This format string tells who has checked in (in a geographical location).
#: ../push/pushhelper.cpp:260
msgid "%1 has checked-in"
msgstr "%1 is aangekomen"

#. TRANSLATORS: This format string tells who has just joined Telegram.
#: ../push/pushhelper.cpp:266
msgid "%1 joined Telegram!"
msgstr "%1 heeft nu ook Telegram!"

#: ../push/pushhelper.cpp:271 ../push/pushhelper.cpp:277
msgid "New login from unrecognized device"
msgstr "Ingelogd op onbekend apparaat"

#. TRANSLATORS: This format string indicates new login of: (device name) at (location).
#: ../push/pushhelper.cpp:276
msgid "%1 @ %2"
msgstr "%1 @ %2"

#: ../push/pushhelper.cpp:281
msgid "updated profile photo"
msgstr "bijgewerkte profielfoto"

#: ../push/pushhelper.cpp:286 ../push/pushhelper.cpp:291
#: ../push/pushhelper.cpp:296
msgid "You have a new message"
msgstr "Je hebt een nieuw bericht"

#~ msgid "not available"
#~ msgstr "niet beschikbaar"

#~ msgid "Incorrect auth code length."
#~ msgstr "Incorrecte authorisatiecodelengte."

#~ msgid "Phone call"
#~ msgstr "Telefoongesprek"

#~ msgid "sent an audio message"
#~ msgstr "heeft een spraakbericht gestuurd"

#~ msgid "sent a photo"
#~ msgstr "heeft een foto gestuurd"

#~ msgid "sent a video"
#~ msgstr "heeft een video gestuurd"

#~ msgid "sent a video note"
#~ msgstr "heeft een video-notitie gestuurd"

#~ msgid "sent a voice note"
#~ msgstr "heeft een spraaknotitie gestuurd"

#~ msgid "joined by invite link"
#~ msgstr "is lid geworden via een link met uitnodiging"

#~ msgid "Image"
#~ msgstr "Afbeelding"

#~ msgid "File"
#~ msgstr "Bestand"

#~ msgid "sent an unknown message: %1"
#~ msgstr "heeft een onbekend bericht gestuurd: %1"
